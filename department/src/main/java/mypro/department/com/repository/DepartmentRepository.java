package mypro.department.com.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mypro.department.com.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Long> {

	public Department findByDepartmentName(String departmentName);

	public Optional<Department> findByDepartmentNameIgnoreCase(String departmentName);
	
}