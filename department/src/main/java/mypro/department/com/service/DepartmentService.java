package mypro.department.com.service;


import java.util.List;
import mypro.department.com.entity.Department;
import mypro.department.com.error.DepartmentNotFoundException;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartmentById(Long departmentId, Department department);

	public Department fetchDepartmentByName(String departmentName) throws DepartmentNotFoundException;
}