package mypro.com.graphQL.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mypro.com.graphQL.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
