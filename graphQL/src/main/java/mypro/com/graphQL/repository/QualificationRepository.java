package mypro.com.graphQL.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import mypro.com.graphQL.entity.Qualification;

public interface QualificationRepository extends JpaRepository<Qualification, Integer>{

}

