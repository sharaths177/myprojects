package mypro.com.graphQL.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mypro.com.graphQL.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>{

}
