package mypro.com.graphQL.resolver;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import javassist.NotFoundException;
import mypro.com.graphQL.entity.Address;
import mypro.com.graphQL.entity.User;
import mypro.com.graphQL.entity.Qualification;
import mypro.com.graphQL.repository.AddressRepository;
import mypro.com.graphQL.repository.UserRepository;
import mypro.com.graphQL.repository.QualificationRepository;



@Component
public class Mutation implements GraphQLMutationResolver {
	private UserRepository uRepo;
	private AddressRepository aRepo;
	private QualificationRepository qRepo;
	@Autowired
	public Mutation(UserRepository uRepo, AddressRepository aRepo, QualificationRepository qRepo) {
		
		this.uRepo = uRepo;
		this.aRepo = aRepo;
		this.qRepo = qRepo;
	}
	
	public Address createAddress(String street,String city,String state,String country,int zip) {
		Address address=new Address();
		address.setCity(city);
		address.setCountry(country);
		address.setStreet(street);
		address.setZip(zip);
		address.setState(state);
		aRepo.save(address);
		return address;
	}
	public Qualification createQualification(String qdetails){
		Qualification qualification=new Qualification();
		qualification.setQdetails(qdetails);
		qRepo.save(qualification);
		return qualification;
		}
	public User createUser(String name,int qualification_id,int address_id) {
		User user =new User();
		user.setName(name);
		user.setAddress(new Address(address_id));
		user.setQualification(new Qualification(qualification_id));
		uRepo.save(user);
		return user;
	}
	public boolean deleteUser(int id) {
		uRepo.deleteById(id);
		aRepo.deleteById(id);
		qRepo.deleteById(id);
		return true;
	}
	public User updateUser( int id,String name) throws NotFoundException {
		Optional<User> optUser = uRepo.findById(id);

		if (optUser.isPresent()) {
			User user = optUser.get();

			if (name != null)
				user.setName(name);
			

			uRepo.save(user);
			return user;
		}

		throw new NotFoundException("Not found user to update!");
	}
}
