package mypro.com.graphQL.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import mypro.com.graphQL.entity.Address;
import mypro.com.graphQL.entity.User;
import mypro.com.graphQL.entity.Qualification;
import mypro.com.graphQL.repository.AddressRepository;
import mypro.com.graphQL.repository.QualificationRepository;

@Component 
public class UserResolver implements GraphQLResolver<User>{
	@Autowired
	private QualificationRepository qRepo;
	private AddressRepository aRepo;
	
	public UserResolver(AddressRepository aRepo,QualificationRepository qRepo) {
		this.aRepo=aRepo;
		this.qRepo=qRepo;
	}
	public Address getAddress(User user) {
		return aRepo.findById(user.getAddress().getaId()).orElseThrow(null);
	}
	public Qualification getQualification(User user) {
		return qRepo.findById(user.getQualification().getQid()).orElseThrow(null);
	}
}
