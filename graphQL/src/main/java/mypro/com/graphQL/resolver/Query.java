package mypro.com.graphQL.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import mypro.com.graphQL.entity.Address;
import mypro.com.graphQL.entity.User;
import mypro.com.graphQL.entity.Qualification;
import mypro.com.graphQL.repository.AddressRepository;
import mypro.com.graphQL.repository.UserRepository;
import mypro.com.graphQL.repository.QualificationRepository;

@Component
public class Query implements GraphQLQueryResolver {
	private UserRepository uRepo;
	private AddressRepository aRepo;
	private QualificationRepository qRepo;

	@Autowired
	public Query(UserRepository uRepo, AddressRepository aRepo, QualificationRepository qRepo) {

		this.uRepo = uRepo;
		this.aRepo = aRepo;
		this.qRepo = qRepo;
	}

	public Iterable<User> findAllUser() {
		return uRepo.findAll();
	}

	public Iterable<Address> findAllAddress() {
		return aRepo.findAll();
	}

	public Iterable<Qualification> findAllQualification() {
		return qRepo.findAll();
	}
}
