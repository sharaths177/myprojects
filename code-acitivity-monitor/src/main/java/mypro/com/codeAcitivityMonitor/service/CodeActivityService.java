package mypro.com.codeAcitivityMonitor.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mypro.com.codeAcitivityMonitor.entity.GroupProjectInfo;


@Service
public class CodeActivityService {
	@Autowired
	RestTemplate restTemplate;

	@Value("${git.repo.groups}")
	private String[] groupIds;
	List<String> projectIds = new ArrayList<String>();
	public static final String get_all_project_details = "https://gitlab.com/api/v4/groups/{groupId}";

	ResponseEntity<Object> commitsInfo;

	public List<String> getProjectIdsByGroupId() throws JsonMappingException, JsonProcessingException {
		for (String groupId : groupIds) {
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> groupInfo = new HttpEntity<String>("groupId", headers);
			ResponseEntity<GroupProjectInfo> projectsInfo = restTemplate.exchange(get_all_project_details,
					HttpMethod.GET, groupInfo, GroupProjectInfo.class, groupId);

			for (int i = 0; i < (projectsInfo.getBody().getProjects()).size(); i++) {
				projectIds.add(String.valueOf(projectsInfo.getBody().getProjects().get(i).getId()));
			}
		}
		getCommitInfoByProjectId();
		return commitList;
	}

	ArrayList<String> commits = new ArrayList<String>();
	List<String> commitList = new ArrayList<String>();
	public void getCommitInfoByProjectId() throws JsonMappingException, JsonProcessingException {
		for (String projectId : projectIds) {
			// System.out.println("Project "+projectId);
			HttpHeaders headers = new HttpHeaders();
			// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<String> projectData = new HttpEntity<String>("projectId", headers);
			commitsInfo = restTemplate.exchange(
					"https://gitlab.com/api/v4/projects/{projectId}/repository/commits?ref_name=master", HttpMethod.GET,
					projectData, Object.class, projectId);
			commits.add(commitsInfo.getBody().toString());
		}
		for (String commit : commits) {
			commitList.add(commit.toString());
		}
	}
}
