package mypro.com.codeAcitivityMonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CodeAcitivityMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeAcitivityMonitorApplication.class, args);
	}
	@Bean
	public RestTemplate getRestTemplate() {
	      return new RestTemplate();
	}
}
