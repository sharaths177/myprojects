package mypro.com.codeAcitivityMonitor.entity;

import java.util.List;

public class GroupProjectInfo {
	Long id;
	String name;
	List<Project> projects;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	@Override
	public String toString() {
		return "GroupProjectInfo [id=" + id + ", name=" + name + ", projects=" + projects + "]";
	}
}
