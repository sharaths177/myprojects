package mypro.com.codeAcitivityMonitor.controller;

import java.util.List;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import mypro.com.codeAcitivityMonitor.service.CodeActivityService;

@RestController
public class CodeActivityController {
	@Autowired
	private CodeActivityService codeActivityService;

	@GetMapping("/commitInfo")
	public List<String> getCommitInfoByProjectId()
			throws JsonMappingException, JsonProcessingException, ParseException {
		return codeActivityService.getProjectIdsByGroupId();
	}
}
